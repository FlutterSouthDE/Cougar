import 'package:flutter/material.dart';

class CougarAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;

  const CougarAppBar({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) => ColoredBox(
        color: const Color(0xFF000000),
        child: SafeArea(
          child: ColoredBox(
            color: const Color(0xFF606060),
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: ColoredBox(
                color: const Color(0xFF394DCD),
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Row(
                      children: [
                        Text(title, style: const TextStyle(color: Color(0xFFFFFFFF))),
                        const Spacer(),
                        const _CougarAppBarAction(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      );

  @override
  Size get preferredSize => const Size.fromHeight(30.0);
}

class _CougarAppBarAction extends StatelessWidget {
  const _CougarAppBarAction({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => const SizedBox(
        width: 24.0,
        child: ColoredBox(
          color: Color(0xFF606060),
          child: Center(),
        ),
      );
}

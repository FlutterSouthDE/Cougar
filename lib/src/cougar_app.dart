import 'package:flutter/widgets.dart';

class CougarApp extends StatelessWidget {
  final Widget home;

  const CougarApp({Key? key, required this.home}) : super(key: key);

  @override
  Widget build(BuildContext context) => home;
}

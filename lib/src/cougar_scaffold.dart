import 'package:flutter/material.dart';

class CougarScaffold extends StatelessWidget {
  final PreferredSizeWidget? appBar;
  final Widget body;

  const CougarScaffold({Key? key, this.appBar, required this.body}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: appBar,
        body: body,
      );
}
